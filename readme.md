# Technistore

Technistore est une application de gestion et suivi du matériel de l'entreprise.
Le magasinier pourra ajouter consulter / ajouter / mettre à jour / supprimer du matériel, il pourra également affecter du matériel à un site (ou chantier).
Le magasinier ajoutera les PV de conformité ou d'étalonnage du matériel, ces informations peuvent être récupérer part le chef de chantier pour constituer son DRT.
# Requirements

- php 8.0 ou +
- composer 2.0 ou +
- symfony 5.3

# Démarrer le projet

`git clone https://gitlab.com/Papoel/technistore.git`
`composer install`
`configurer une base de donnée dans un fichier .env.local`
`lancer le script composer fixtures_start`
`symfony serve -d`

### Test des différentes interfaces

 Interface  | Email                     | Password     |
 ---------- | ------------------------- | ------------ |
 User       | user@technisonic.fr       | password |
 Magasinier | magasinier@technisonic.fr | password |
 Admin      | admin@technisonic.fr      | password |
