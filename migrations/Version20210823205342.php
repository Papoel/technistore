<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210823205342 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE categorie (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE chantier (id INT AUTO_INCREMENT NOT NULL, otp VARCHAR(255) NOT NULL, numero_affaire VARCHAR(255) DEFAULT NULL, interlocuteur VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE images (id INT AUTO_INCREMENT NOT NULL, categorie_id INT NOT NULL, name VARCHAR(255) DEFAULT NULL, INDEX IDX_E01FBE6ABCF5E72D (categorie_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE materiels (id INT AUTO_INCREMENT NOT NULL, affectation_id INT DEFAULT NULL, utilisateur_id INT DEFAULT NULL, categorie_id INT DEFAULT NULL, reference VARCHAR(255) NOT NULL, marque VARCHAR(255) NOT NULL, type VARCHAR(10) NOT NULL, dimension VARCHAR(255) NOT NULL, frequence INT DEFAULT NULL, pv VARCHAR(255) DEFAULT NULL, colisage VARCHAR(255) DEFAULT NULL, observations LONGTEXT DEFAULT NULL, version INT DEFAULT NULL, calibrated_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', end_calibrated_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', is_avaible TINYINT(1) NOT NULL, is_in_verif TINYINT(1) NOT NULL, quantity INT NOT NULL, UNIQUE INDEX UNIQ_9C1EBE696D0ABA22 (affectation_id), INDEX IDX_9C1EBE69FB88E14F (utilisateur_id), INDEX IDX_9C1EBE69BCF5E72D (categorie_id), FULLTEXT INDEX IDX_9C1EBE695A6F91CE8CDE57298A5758F2 (marque, type, colisage), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE sites (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, palier VARCHAR(255) DEFAULT NULL, tranche INT DEFAULT NULL, adress VARCHAR(255) DEFAULT NULL, postal_code INT DEFAULT NULL, town VARCHAR(255) DEFAULT NULL, telephone VARCHAR(10) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE technique (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, email VARCHAR(180) NOT NULL, roles LONGTEXT NOT NULL COMMENT \'(DC2Type:json)\', password VARCHAR(255) NOT NULL, firstname VARCHAR(255) NOT NULL, lastname VARCHAR(255) NOT NULL, is_autorised TINYINT(1) DEFAULT NULL, UNIQUE INDEX UNIQ_8D93D649E7927C74 (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE images ADD CONSTRAINT FK_E01FBE6ABCF5E72D FOREIGN KEY (categorie_id) REFERENCES categorie (id)');
        $this->addSql('ALTER TABLE materiels ADD CONSTRAINT FK_9C1EBE696D0ABA22 FOREIGN KEY (affectation_id) REFERENCES sites (id)');
        $this->addSql('ALTER TABLE materiels ADD CONSTRAINT FK_9C1EBE69FB88E14F FOREIGN KEY (utilisateur_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE materiels ADD CONSTRAINT FK_9C1EBE69BCF5E72D FOREIGN KEY (categorie_id) REFERENCES categorie (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE images DROP FOREIGN KEY FK_E01FBE6ABCF5E72D');
        $this->addSql('ALTER TABLE materiels DROP FOREIGN KEY FK_9C1EBE69BCF5E72D');
        $this->addSql('ALTER TABLE materiels DROP FOREIGN KEY FK_9C1EBE696D0ABA22');
        $this->addSql('ALTER TABLE materiels DROP FOREIGN KEY FK_9C1EBE69FB88E14F');
        $this->addSql('DROP TABLE categorie');
        $this->addSql('DROP TABLE chantier');
        $this->addSql('DROP TABLE images');
        $this->addSql('DROP TABLE materiels');
        $this->addSql('DROP TABLE sites');
        $this->addSql('DROP TABLE technique');
        $this->addSql('DROP TABLE user');
    }
}
