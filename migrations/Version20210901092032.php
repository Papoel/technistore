<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210901092032 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE article_test ADD affectation_id INT DEFAULT NULL, ADD utilisateur_id INT DEFAULT NULL, ADD categorie_id INT DEFAULT NULL, ADD reference VARCHAR(255) NOT NULL, ADD marque VARCHAR(255) NOT NULL, ADD type VARCHAR(10) NOT NULL, ADD dimension VARCHAR(255) NOT NULL, ADD frequence INT DEFAULT NULL, ADD pv VARCHAR(255) DEFAULT NULL, ADD colisage VARCHAR(255) DEFAULT NULL, ADD observations LONGTEXT DEFAULT NULL, ADD version INT DEFAULT NULL, ADD end_calibrated_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', ADD is_in_verif TINYINT(1) NOT NULL, ADD quantity INT NOT NULL, DROP title, DROP content, DROP slug, DROP created_at, DROP edited_at, CHANGE published_at calibrated_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', CHANGE is_published is_avaible TINYINT(1) NOT NULL');
        $this->addSql('ALTER TABLE article_test ADD CONSTRAINT FK_683DF90D6D0ABA22 FOREIGN KEY (affectation_id) REFERENCES sites (id)');
        $this->addSql('ALTER TABLE article_test ADD CONSTRAINT FK_683DF90DFB88E14F FOREIGN KEY (utilisateur_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE article_test ADD CONSTRAINT FK_683DF90DBCF5E72D FOREIGN KEY (categorie_id) REFERENCES categorie (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_683DF90D6D0ABA22 ON article_test (affectation_id)');
        $this->addSql('CREATE INDEX IDX_683DF90DFB88E14F ON article_test (utilisateur_id)');
        $this->addSql('CREATE INDEX IDX_683DF90DBCF5E72D ON article_test (categorie_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE article_test DROP FOREIGN KEY FK_683DF90D6D0ABA22');
        $this->addSql('ALTER TABLE article_test DROP FOREIGN KEY FK_683DF90DFB88E14F');
        $this->addSql('ALTER TABLE article_test DROP FOREIGN KEY FK_683DF90DBCF5E72D');
        $this->addSql('DROP INDEX UNIQ_683DF90D6D0ABA22 ON article_test');
        $this->addSql('DROP INDEX IDX_683DF90DFB88E14F ON article_test');
        $this->addSql('DROP INDEX IDX_683DF90DBCF5E72D ON article_test');
        $this->addSql('ALTER TABLE article_test ADD title VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, ADD content LONGTEXT CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, ADD slug VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, ADD created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', ADD published_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', ADD edited_at DATETIME DEFAULT NULL, ADD is_published TINYINT(1) NOT NULL, DROP affectation_id, DROP utilisateur_id, DROP categorie_id, DROP reference, DROP marque, DROP type, DROP dimension, DROP frequence, DROP pv, DROP colisage, DROP observations, DROP version, DROP calibrated_at, DROP end_calibrated_at, DROP is_avaible, DROP is_in_verif, DROP quantity');
    }
}
