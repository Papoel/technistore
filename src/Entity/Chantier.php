<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\ChantierRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ChantierRepository::class)
 */
#[ApiResource]
class Chantier
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $OTP;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $numero_affaire;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $interlocuteur;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getOTP(): ?string
    {
        return $this->OTP;
    }

    public function setOTP(string $OTP): self
    {
        $this->OTP = $OTP;

        return $this;
    }

    public function getNumeroAffaire(): ?string
    {
        return $this->numero_affaire;
    }

    public function setNumeroAffaire(?string $numero_affaire): self
    {
        $this->numero_affaire = $numero_affaire;

        return $this;
    }

    public function getInterlocuteur(): ?string
    {
        return $this->interlocuteur;
    }

    public function setInterlocuteur(?string $interlocuteur): self
    {
        $this->interlocuteur = $interlocuteur;

        return $this;
    }
}
