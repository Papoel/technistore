<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\MaterielsRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=MaterielsRepository::class)
 * @ORM\Table(name="materiels", indexes={@ORM\Index(columns={"marque", "type", "colisage"}, flags={"fulltext"})})
 */
#[ApiResource]
class Materiels
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $reference;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $marque;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private $type;

    /**
     * @ORM\Column(type="string")
     */
    private $dimension;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $frequence;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $PV;

    /**
     * @ORM\OneToOne(targetEntity=Sites::class, inversedBy="materiels", cascade={"persist", "remove"})
     */
    private $affectation;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $colisage;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $observations;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $version;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="materiels")
     */
    private $utilisateur;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $calibrated_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $end_calibrated_at;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isAvaible;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isInVerif;

    /**
     * @ORM\Column(type="integer")
     * @Assert\PositiveOrZero
     */
    private $quantity;

    /**
     * @ORM\ManyToOne(targetEntity=Categorie::class, inversedBy="materiels")
     */
    private $categorie;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getReference(): ?string
    {
        return $this->reference;
    }

    public function setReference(string $reference): self
    {
        $this->reference = $reference;

        return $this;
    }

    public function getMarque(): ?string
    {
        return $this->marque;
    }

    public function setMarque(string $marque): self
    {
        $this->marque = $marque;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getDimension(): ?string
    {
        return $this->dimension;
    }

    public function setDimension(string $dimension): self
    {
        $this->dimension = $dimension;

        return $this;
    }

    public function getFrequence(): ?int
    {
        return $this->frequence;
    }

    public function setFrequence(int $frequence): self
    {
        $this->frequence = $frequence;

        return $this;
    }

    public function getPV(): ?string
    {
        return $this->PV;
    }

    public function setPV(?string $PV): self
    {
        $this->PV = $PV;

        return $this;
    }

    public function getAffectation(): ?Sites
    {
        return $this->affectation;
    }

    public function setAffectation(?Sites $affectation): self
    {
        $this->affectation = $affectation;

        return $this;
    }

    public function getColisage(): ?string
    {
        return $this->colisage;
    }

    public function setColisage(?string $colisage): self
    {
        $this->colisage = $colisage;

        return $this;
    }

    public function getObservations(): ?string
    {
        return $this->observations;
    }

    public function setObservations(?string $observations): self
    {
        $this->observations = $observations;

        return $this;
    }

    public function getVersion(): ?int
    {
        return $this->version;
    }

    public function setVersion(?int $version): self
    {
        $this->version = $version;

        return $this;
    }

    public function getUtilisateur(): ?User
    {
        return $this->utilisateur;
    }

    public function setUtilisateur(?User $utilisateur): self
    {
        $this->utilisateur = $utilisateur;

        return $this;
    }

    public function getCalibratedAt(): ?\DateTime
    {
        return $this->calibrated_at;
    }

    public function setCalibratedAt(?\DateTime $calibrated_at): self
    {
        $this->calibrated_at = $calibrated_at;

        return $this;
    }

    public function getEndCalibratedAt(): ?\DateTime
    {
        return $this->end_calibrated_at;
    }

    public function setEndCalibratedAt(?\DateTime $end_calibrated_at): self
    {
        $this->end_calibrated_at = $end_calibrated_at;

        return $this;
    }

    public function getIsAvaible(): ?bool
    {
        return $this->isAvaible;
    }

    public function setIsAvaible(bool $isAvaible): self
    {
        $this->isAvaible = $isAvaible;

        return $this;
    }

    public function getIsInVerif(): ?bool
    {
        return $this->isInVerif;
    }

    public function setIsInVerif(bool $isInVerif): self
    {
        $this->isInVerif = $isInVerif;

        return $this;
    }

    public function getQuantity(): ?int
    {
        return $this->quantity;
    }

    public function setQuantity(int $quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }

    public function getCategorie(): ?Categorie
    {
        return $this->categorie;
    }

    public function setCategorie(?Categorie $categorie): self
    {
        $this->categorie = $categorie;

        return $this;
    }
}
