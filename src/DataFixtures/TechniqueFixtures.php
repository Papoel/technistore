<?php

namespace App\DataFixtures;

use App\Entity\Technique;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;

class TechniqueFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        // - Création des techniques
        for ($i = 1; $i <= 4; $i++) {
            $technique = new Technique();
            $value = ['ressuage', 'magnetoscopie', 'ultrason', 'radiographie'];
            $technique->setName($value[$i - 1]);

            $manager->persist($technique);
        }

        $manager->flush();
    }
}
