<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class UserFixtures extends Fixture
{
    private $passwordHasher;

    public function __construct(UserPasswordHasherInterface $passwordHasher)
    {
        $this->passwordHasher = $passwordHasher;
    }

    public function load(ObjectManager $manager)
    {
        // user
        $user = new User();
        $user->setFirstname('Perseval');
        $user->setLastname('Papoel');
        $user->setEmail("user@technisonic.fr");
        $user->setPassword($this->passwordHasher->hashPassword($user, 'password'));
        $user->setRoles(['ROLE_USER']);
        $user->setIsAutorised(0);

        $manager->persist($user);

        // admin
        $user = new User();
        $user->setFirstname('Pascal');
        $user->setLastname('Briffard');
        $user->setEmail("admin@technisonic.fr");
        $user->setPassword($this->passwordHasher->hashPassword($user, 'password'));
        $user->setRoles(['ROLE_ADMIN', 'ROLE_USER', 'ROLE_MAGASINIER']);
        $user->setIsAutorised(1);

        $manager->persist($user);
        
        // magasinier
        $user = new User();
        $user->setFirstname('Romuald');
        $user->setLastname('Claes');
        $user->setEmail("magasinier@technisonic.fr");
        $user->setPassword($this->passwordHasher->hashPassword($user, 'password'));
        $user->setRoles(['ROLE_MAGASINIER', 'ROLE_USER']);
        $user->setIsAutorised(1);

        $manager->persist($user);

        $manager->flush();
    }
}
