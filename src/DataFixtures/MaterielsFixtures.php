<?php

namespace App\DataFixtures;

use Faker;
use App\Entity\Materiels;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class MaterielsFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $faker = Faker\Factory::create('fr_FR');
        for ($i = 1; $i <= 25; $i++) {
            $materiel = new Materiels();
            $materiel->setReference($faker->bothify('##-?????'));
            $materiel->setMarque(
                $faker->randomElement(
                    ['Sofranel', 'Krautkramer', 'General Electric', 'Olympus']
                )
            );
            $materiel->setType(
                $faker->randomElement(
                    ['TG101HR', 'MDP4-10', 'SLP5-5D', 'DP4-24', 'QC-5', 'MB4-S']
                )
            );
            $materiel->setDimension($faker->randomElement(['Ø5', 'Ø10', 'Ø24', '8x9']));
            $materiel->setFrequence($faker->randomElement([1, 2, 5, 10, 24]));
            $materiel->setColisage($faker->randomElement(
                [
                    'Gravelines',
                    'Penly',
                    'Paluel',
                    'Flamanville',
                    'Chooz',
                    'Cattenom',
                    'Nogent',
                    'Dampierre',
                    'Saint Laurent',
                    'Chinon',
                    'Belleville',
                    'Civaux',
                    'Blayais',
                    'Golfech',
                    'Bugey',
                    'Saint Alban',
                    'Cruas',
                    'Tricastin',
                    'Manom',
                    'Dunkerque'
                ]
            ));
            $materiel->setCalibratedAt($faker->DateTimeBetween('-2 years', '+2 days'));
            $materiel->setEndCalibratedAt($faker->DateTimeBetween('-2 years', '+1 years'));
            $materiel->setIsAvaible($faker->boolean());
            $materiel->setIsInVerif($faker->boolean());
            $materiel->setQuantity($faker->numberBetween(0, 100));
            // Recuperer les catégorie par référence
            $categorie = $this->getReference('categorie_' .$faker->numberBetween(1, 7));
            $materiel->SetCategorie($categorie);

            $manager->persist($materiel);
        }

        $manager->flush();
    }

    // Lorsque mes fixtures dépendendent d'autre fixtures je dois ajouter implements DependentFixtureInterface
    // puis la mèthode getDependencies, elle doit retoutner le tableau des dépendances (CategorieFixtures ici)
    public function getDependencies()
    {
        return [
            CategorieFixtures::class
        ];
    }
}
