<?php

namespace App\DataFixtures;

use App\Entity\Sites;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;

class SitesFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $cnpe = [
            [
                'name' => 'Gravelines',
                'palier' => 'CP0',
                'tranche' => 6,
                'ville' => 'Gravelines'
            ],
            [
                'name' => 'Penly',
                'palier' => '',
                'tranche' => 2,
                'ville' => 'Penly'
            ],
            [
                'name' => 'Paluel',
                'palier' => '',
                'tranche' => 4,
                'ville' => 'Paluel'
            ],
            [
                'name' => 'Flamanville',
                'palier' => '',
                'tranche' => 3,
                'ville' => 'Flamanville'
            ],
            [
                'name' => 'Chooz',
                'palier' => '',
                'tranche' => 2,
                'ville' => 'Chooz'
            ],
            [
                'name' => 'Cattenom',
                'palier' => '',
                'tranche' => 4,
                'ville' => 'Cattenom'
            ],
            [
                'name' => 'Nogent',
                'palier' => '',
                'tranche' => 2,
                'ville' => 'Nogent'
            ],
            [
                'name' => 'Dampierre',
                'palier' => '',
                'tranche' => 4,
                'ville' => 'Dampierre'
            ],
            [
                'name' => 'Saint Laurent',
                'palier' => '',
                'tranche' => 2,
                'ville' => 'Saint Laurent'
            ],
            [
                'name' => 'Chinon',
                'palier' => '',
                'tranche' => 4,
                'ville' => 'Chinon'
            ],
            [
                'name' => 'Belleville',
                'palier' => '',
                'tranche' => 4,
                'ville' => 'Belleville'
            ],
            [
                'name' => 'Civaux',
                'palier' => '',
                'tranche' => 2,
                'ville' => 'Civaux'
            ],
            [
                'name' => 'Blayais',
                'palier' => '',
                'tranche' => 3,
                'ville' => 'Blayais'
            ],
            [
                'name' => 'Golfech',
                'palier' => '',
                'tranche' => 2,
                'ville' => 'Golfech'
            ],
            [
                'name' => 'Bugey',
                'palier' => '',
                'tranche' => 5,
                'ville' => 'Bugey'
            ],
            [
                'name' => 'Saint Alban',
                'palier' => '',
                'tranche' => 4,
                'ville' => 'Saint Alban'
            ],
            [
                'name' => 'Cruas',
                'palier' => '',
                'tranche' => 4,
                'ville' => 'Cruas'
            ],
            [
                'name' => 'Tricastin',
                'palier' => '',
                'tranche' => 4,
                'ville' => 'Tricastin'
            ],
            [
                'name' => 'Manom',
                'palier' => '',
                'tranche' => 0,
                'ville' => 'Manom'
            ],
            [
                'name' => 'Dunkerque',
                'palier' => '',
                'tranche' => 0,
                'ville' => 'Dunkerque'
            ]
        ];

        foreach ($cnpe as $value) {
            $site = new Sites();
            $site->setName($value['name']);
            $site->setTown($value['ville']);
            $site->setTranche($value['tranche']);
            $site->setPalier($value['palier']);

            $manager->persist($site);
        }


        $manager->flush();
    }
}
