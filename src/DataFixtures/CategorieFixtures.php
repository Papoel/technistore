<?php
namespace App\DataFixtures;

use Faker;
use App\Entity\Images;
use App\Entity\Categorie;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;


class CategorieFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $faker = Faker\Factory::create('fr_FR');

        $categories = [
            1 => ['name' => 'palpeur'],
            2 => ['name' => 'poste ultrason'],
            3 => ['name' => 'luxmetre'],
            4 => ['name' => 'thermometre'],
            5 => ['name' => 'cales'],
            6 => ['name' => 'cable'],
            7 => ['name' => 'produits'],
        ];

        foreach($categories as $key => $value) {
            $categorie = new Categorie();
            $categorie->setName($value['name']);

            // $manager->persist($categorie);

            // Enregistrer la catégorie dans une référence
            // pour utiliser ces données dans d'autre jeu de fixtures 
            $this->addReference('categorie_' .$key, $categorie);

            $manager->persist($categorie);
        }

        $manager->flush();
    }

}