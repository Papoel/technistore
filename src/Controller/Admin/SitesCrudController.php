<?php

namespace App\Controller\Admin;

use App\Entity\Sites;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class SitesCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Sites::class;
    }

    /*
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id'),
            TextField::new('title'),
            TextEditorField::new('description'),
        ];
    }
    */
}
