<?php

namespace App\Controller\Admin;

use App\Entity\Test;
use App\Entity\User;
use App\Entity\Sites;
use App\Entity\Chantier;
use App\Entity\Categorie;
use App\Entity\Materiels;
use App\Entity\Technique;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;

class AdminDashboardController extends AbstractDashboardController
{
    /**
     * @Route("/admin", name="admin")
     */
    public function index(): Response
    {
        // return parent::index();
        return $this->render('admin/dashboard.html.twig');
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('Technistore');
    }

    public function configureMenuItems(): iterable
    {
        yield MenuItem::linktoDashboard('Dashboard', 'fa fa-home');
        yield MenuItem::linkToCrud('Les Utilisateurs', 'fas fa-users', User::class);
        yield MenuItem::linkToCrud('Le Matériels', 'fas fa-warehouse', Materiels::class);
        yield MenuItem::linkToCrud('Les Catégories', 'fas fa-list', Categorie::class);
        yield MenuItem::linkToCrud('Les Chantiers', 'fas fa-radiation', Chantier::class);
        yield MenuItem::linkToCrud('Les Sites', 'fas fa-globe', Sites::class);
        yield MenuItem::linkToCrud('Les Techniques', 'fas fa-dice-d20', Technique::class);    }
}
