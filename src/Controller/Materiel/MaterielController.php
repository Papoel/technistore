<?php

namespace App\Controller\Materiel;

use App\Entity\Materiels;
use App\Form\MaterielFormType;
use App\Form\SearchMaterielType;
use App\Repository\MaterielsRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class MaterielController extends AbstractController
{

    private $repoMateriels;

    function __construct( MaterielsRepository $repoMateriels )
    {
        $this->repoMateriels = $repoMateriels;
    }

    //! READ_ALL 
    // Route principal affichant tous les produits pour le magasinier (action visible)
    #[Route('/materiels', name: 'app_materiels_index')]
    public function index(PaginatorInterface $paginator, Request $request, MaterielsRepository $repoMateriels): Response
    {
        //? Mise en place de la pagination
        $data = $this->repoMateriels->findAll();

        $materiels = $paginator->paginate(
            $data, 
            $request->query->getInt('page', 1), 
            20
        );

        // Executer une recherche par mot clé
        $form = $this->createForm(SearchMaterielType::class);
        $search = $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {

            $materiels = $repoMateriels->search(
                $search->get('mots')->getData(),
                $search->get('categorie')->getData()
            );
        }
        
        // dd($materiels);

        return $this->render('materiels/index.html.twig', [
            'materiels' => $materiels,
            'form' => $form->createView()
        ]);
    }

    //!TESTS ONLY
    #[Route('/materiels/test', name: 'app_materiels_test')]
    #[IsGranted('ROLE_MAGASINIER')]
    public function pageTest(Request $request, MaterielsRepository $repoMateriels): Response
    {
        $materiels = $this->repoMateriels->findAll();

        // Executer une recherche par mot clé
        $form = $this->createForm(SearchMaterielType::class);
        $search = $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {

            $materiels = $repoMateriels->search(
                $search->get('mots')->getData(),
                $search->get('categorie')->getData()
            );
        }
        

        return $this->render('materiels/search.html.twig', [
            'materiels' => $materiels,
            'form' => $form->createView(),
        ]);
    }

    //! READ_ONE 
    // Route détaillant un produit
    #[Route('/materiels/{id<[0-9]+>}', methods: 'GET', name: 'app_materiel_show')]
    #[IsGranted('ROLE_USER')]
    public function show(Materiels $materiel): Response
    {
        return $this->render('materiels/show.html.twig', compact('materiel'));
    
    }

    //! CREATE 
    // Ajout d'un nouveau matériel
    #[Route('/materiels/create', methods:['GET', 'POST'], name:'app_materiels_create')]
    #[IsGranted('ROLE_MAGASINIER')]
    public function create(Request $request, EntityManagerInterface $em): Response
    {
        $materiel = new Materiels();

        $form = $this->createForm(MaterielFormType::class, $materiel);
        
        $form->handleRequest($request);

        if ($form -> isSubmitted() && $form->isValid()) {
            
            $em->persist($materiel);
            $em->flush();

            $this->addFlash('success', 'Matériel crée avec succès : ');

            return $this->redirectToRoute('app_materiel_show', ['id' => $materiel->getId()]);
        }

        return $this->renderForm('materiels/create.html.twig', [
            'form' => $form
        ]);
    }

    //! UPDATE
    // Modification ou mise à jour d'un produit
    #[Route('/materiels/{id<[0-9]+>}/edit', methods:['GET', 'POST'], name:'app_materiels_edit')]
    #[IsGranted('ROLE_MAGASINIER')]
    public function edit(Materiels $materiel, Request $request, EntityManagerInterface $em): Response 
    {
        
        $form = $this->createForm(MaterielFormType::class, $materiel);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $em->flush();

            $this->addFlash('success', 'Modification sauvegardé');

            return $this->redirectToRoute('app_materiel_show', ['id' => $materiel->getId()]);
        }

        return $this->renderForm('materiels/edit.html.twig', [
            'materiel' => $materiel, 
            'form' => $form
        ]);
    }

    //! DELETE
    // Effacer un matériel de la base de donnée
    #[Route('/materiels/{id<[0-9]+>}/delete', methods:'POST', name:'app_materiels_delete')]
    #[IsGranted('ROLE_MAGASINIER')]
    public function delete(Request $request, Materiels $materiel, EntityManagerInterface $em): Response
    {
        if ($this->isCsrfTokenValid('materiel_deletion_' . $materiel->getId(), $request->request->get('csrf_token')) ) {
            $em->remove($materiel);
            $em->flush();

            $this->addFlash('danger', 'Matériel '.$materiel->getMarque(). ' ' .$materiel->getType() .'a été supprimé !');
        } 
            return $this->redirectToRoute('app_materiels_index');

    }


}
