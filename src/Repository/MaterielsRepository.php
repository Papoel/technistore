<?php

namespace App\Repository;

use Doctrine\ORM\Query;
use App\Entity\Materiels;
use App\Entity\MaterielSearch;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

/**
 * @method Materiels|null find($id, $lockMode = null, $lockVersion = null)
 * @method Materiels|null findOneBy(array $criteria, array $orderBy = null)
 * @method Materiels[]    findAll()
 * @method Materiels[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MaterielsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Materiels::class);
    }

    /**
     * Rechercher le materiel en fonction du formulaire de rechercehe
     * @return void
     */
    public function search($mots = null, $categorie = null) {
        $query = $this->createQueryBuilder('m');
        if ($mots != null) {
            $query->Where('MATCH_AGAINST(m.marque, m.type, m.colisage) AGAINST(:mots boolean)>0')
                  ->setParameter('mots', $mots);
        }

        if ($categorie != null) {
            $query->leftJoin('m.categorie', 'c');
            $query->andWhere('c.id = :id')
                  ->setParameter('id', $categorie);
        }

        return $query->getQuery()->getResult();
    }

    // /**
    //  * @return Materiels[] Returns an array of Materiels objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Materiels
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
