<?php

namespace App\Form;

use App\Entity\User;
use App\Entity\Sites;
use App\Entity\Categorie;
use App\Entity\Materiels;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class MaterielFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('marque', TextType::class, [
                'attr' => ['autofocus' => true],
                'help' => 'Entrer le marque du matériel'
                ])
            ->add('type', TextType::class, [
                'required'   => true,
                'help' => 'Entrer le type du matériel'
                ])
            ->add('reference', TextType::class, [
                'required'   => true,
                'help' => 'Entrer la référence du matériel'
                ])
            ->add('dimension', TextType::class, [
                'required'   => true,
                'help' => 'Entrer les dimmesnions ou le Ø'
                ])
            ->add('frequence', NumberType::class,[
                'required'   => false,
                'help' => 'Entrer la fréquence en Mhz'
                ])
            ->add('pv',  FileType::class, [ 
                'help' => "Ajouter le certificat de conformité ou le PV d'étalonnage",
                'required'   => false,
                'label' => 'PV de conformité' 
                ])

            ->add('colisage', TextType::Class, [
                'required'   => false,
                'help' => 'Indiquer ou se trouve le matériel est actuellement'
                ])
            
            ->add('colisage', EntityType::Class, [
                'class' => Sites::class,
                'choice_label' => 'name',
                'label' => 'Colisage du matériel'
            ])

            // ->add('affectation', EntityType::class, [
            //     'class' => User::class,
            //     'choice_label' => 'FullName',
            //     'label' => 'Materiel affceté'
            // ])
            
            ->add('observations', TextareaType::class, [
                'help' => 'Ajouter une notes',
                'required'   => false,
                'attr' => [
                    'class' => 'text-danger',
                    'placeholder' => 'Inscrivez ici vos observations ou informations',
                    'rows' => 5
                    ] 
                ])
            ->add('categorie', EntityType::class, [
                'class' => Categorie::class,
                'required' => true,
                'choice_label' => 'name',
                'attr' => [
                    'class' => 'text-capitalize',
                ]
            ])
            ->add('calibrated_at', dateType::class, [
                'required' => false,
                'label' => "Date d'étalonnage" 
             ])
            ->add('end_calibrated_at', dateType::class, [
                'required' => false,
                'label' => "Fin de validité" 
             ])
            ->add('isAvaible', null, [  
                'row_attr' => [
                    'class' => 'form-switch form-check'
                ],
                'label' => "Matériel disponible", 
                'required'   => false
                ])
            ->add('isInVerif', null, [
                'row_attr' => [
                    'class' => 'form-switch form-check'
                ],
                'label' => 'Matériel en vérification/étalonnage',
                'required'   => false
            ])
            ->add('quantity', NumberType::class, [
                'attr' => [
                    'min' => 0
                ],
                'html5' => true,
                'label' => 'Quantité',
                'required'   => true,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Materiels::class
        ]);
    }
}
