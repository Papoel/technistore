<?php

namespace App\Form;

use App\Entity\Categorie;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SearchType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class SearchMaterielType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
    $builder
            ->add('mots', SearchType::class, [
                'required' => false,
                'label' => false,
                'attr' => [
                    'class' => 'form-control perso_dark_color',
                    'placeholder' => 'Entrer un ou plusieur mot clés (marque, type, affectation)'
                ]
            ])

            ->add('categorie', EntityType::class, [
                'required' => false,
                'class' => Categorie::class,
                'label' => false,
                'attr' => [
                    'class' => 'form-control text-capitalize perso_dark_color fw-bold',
                    'placeholder' => 'Chercher par catégorie'
                ]
            ])

            ->add('submit', SubmitType::class, [
                'label' => 'Rechercher',
                'attr' => [
                    'class' => 'btn-outline-success d-block mx-auto w-50 mt-3'
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'method' => 'GET',
            'csrf_protection' => false
        ]);
    }

    public function getBlockPrefix()
    {
        return '';
    }
}
